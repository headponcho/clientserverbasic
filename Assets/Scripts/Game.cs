﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    public interface IGameLoop {
        bool Init(string[] args);
        void Shutdown();

        void Update(float deltaTime);
        void FixedUpdate();
        void LateUpdate();
    }

    public static Game game;

    [SerializeField] GameObject ConsoleInGamePrefab;

    List<Type> m_RequestedGameLoopTypes = new List<System.Type>();
    List<string[]> m_RequestedGameLoopArguments = new List<string[]>();
    List<IGameLoop> m_GameLoops = new List<IGameLoop>();

    bool m_IsHeadless;

    private void Awake() {
        List<string> commandLineArguments = new List<string>(System.Environment.GetCommandLineArgs());

#if UNITY_STANDALONE_LINUX
        m_IsHeadless = true;
#else
        m_IsHeadless = commandLineArguments.Contains("-batchmode");
#endif

        if (m_IsHeadless) {
#if UNITY_STANDALONE_WIN
            string consoleTitle = Application.productName;
            consoleTitle += " [" + System.Diagnostics.Process.GetCurrentProcess().Id + "]";
            ConsoleTextWin console = new ConsoleTextWin(consoleTitle);
#elif UNITY_STANDALONE_LINUX
            ConsoleNullUI console = new ConsoleNullUI();
#endif
            Console.Init(console);

            RequestGameLoop(typeof(ServerGameLoop), commandLineArguments.ToArray());

            // Set tick rate of dedicated server
            Application.targetFrameRate = NetworkConfig.tickRate;
            QualitySettings.vSyncCount = 0;

            Console.Write("Headless game server started.");
        } else {
            GameObject consoleGO = Instantiate(ConsoleInGamePrefab, FindObjectOfType<Canvas>().transform);
            Console.Init(consoleGO.GetComponent<ConsoleInGame>());

            RequestGameLoop(typeof(ClientGameLoop), commandLineArguments.ToArray());
        }

#if UNITY_EDITOR
        ClearRequestedGameLoops();
        string[] serverLoopArgs = new string[] { "-listen" };
        string[] clientLoopArgs = new string[] { "-listen" };
        RequestGameLoop(typeof(ServerGameLoop), serverLoopArgs);
        RequestGameLoop(typeof(ClientGameLoop), clientLoopArgs);
#endif

        GameDebug.Init();
    }

    // Use this for initialization
    void Start () {
		
	}

    private void OnDestroy() {
        GameDebug.Shutdown();
        Console.Shutdown();
    }

    private void OnApplicationQuit() {
        ShutdownGameLoops();
    }

    // Update is called once per frame
    void Update () {
        if (m_RequestedGameLoopTypes.Count > 0) {
            ShutdownGameLoops();

            bool initSucceeded = false;

            for (int i = 0; i < m_RequestedGameLoopTypes.Count; i++) {
                try {
                    IGameLoop gameLoop = (IGameLoop)System.Activator.CreateInstance(m_RequestedGameLoopTypes[i]);
                    initSucceeded = gameLoop.Init(m_RequestedGameLoopArguments[i]);
                    if (!initSucceeded) { break; }

                    m_GameLoops.Add(gameLoop);
                    GameDebug.Log("Game loop " + m_RequestedGameLoopTypes[i] + " started.");
                } catch (System.Exception e) {
                    GameDebug.LogError(string.Format("Game loop initialization threw exception : ({0})\n{1}", e.Message, e.StackTrace));
                }
            }

            if (!initSucceeded) {
                ShutdownGameLoops();
                GameDebug.Log("Game loop initialiation failed.");
            }

            ClearRequestedGameLoops();
        }

        foreach (IGameLoop gameLoop in m_GameLoops) {
            gameLoop.Update(Time.deltaTime);
        }
	}

    private void FixedUpdate() {
        foreach (IGameLoop gameLoop in m_GameLoops) {
            gameLoop.FixedUpdate();
        }
    }

    private void LateUpdate() {
        foreach (IGameLoop gameLoop in m_GameLoops) {
            gameLoop.LateUpdate();
        }
    }

    void RequestGameLoop(Type gameLoopType, string[] args) {
        m_RequestedGameLoopTypes.Add(gameLoopType);
        m_RequestedGameLoopArguments.Add(args);
        GameDebug.Log("Game loop " + gameLoopType + " requested.");
    }

    void ClearRequestedGameLoops() {
        GameDebug.Log("Requested game loops cleared.");
        m_RequestedGameLoopTypes.Clear();
        m_RequestedGameLoopArguments.Clear();
    }

    void ShutdownGameLoops() {
        foreach (IGameLoop gameLoop in m_GameLoops) {
            gameLoop.Shutdown();
        }
        m_GameLoops.Clear();
    }
}
