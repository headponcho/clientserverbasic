﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour {

    [SerializeField] float speed = 4f;

    Rigidbody m_Rigidbody;

	// Use this for initialization
	void Start () {
        GameDebug.Log("Rigidbody reference set!");
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.Pause();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Move(UserCommand input) {
        m_Rigidbody.Unpause();

        Vector3 moveDir = Vector3.zero;

        if (input.forward) {
            moveDir.z = 1f;
        } else if (input.backward) {
            moveDir.z = -1f;
        }
        
        if (input.left) {
            moveDir.x = -1f;
        } else if (input.right) {
            moveDir.x = 1f;
        }

        moveDir = moveDir.normalized;
        moveDir *= speed;

        moveDir.y = m_Rigidbody.velocity.y;

        m_Rigidbody.velocity = moveDir;

        Physics.Simulate(Time.fixedDeltaTime);

        m_Rigidbody.Pause();
    }

    public void SetPosition(Vector3 pos) {
        if (!m_Rigidbody) {
            GameDebug.LogWarning("No rigidbody reference exists.");
            return;
        }

        GameDebug.Log(string.Format("Rigidbody set to {0}, {1}, {2}", pos.x, pos.y, pos.z));
        m_Rigidbody.Unpause();
        m_Rigidbody.position = pos;
        Physics.Simulate(Time.fixedDeltaTime);
        m_Rigidbody.Pause();
    }

    public void SetPosition(float x, float y, float z) {
        SetPosition(new Vector3(x, y, z));
    }
}
