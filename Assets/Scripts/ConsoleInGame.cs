﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleInGame : MonoBehaviour, IConsoleUI {

    [SerializeField] GameObject consoleScrollView;
    [SerializeField] Text consoleText;
    [SerializeField] Scrollbar consoleScrollbar;

    // In Unity editor, X-position keeps getting set to center for some reason. 
    [SerializeField] RectTransform textRectTransform;

    int m_MaxLines = 20;
    int m_LineCount;

    public ConsoleInGame() {

    }

    public void Init() {
        GetComponent<RectTransform>().position.Set(0f, 0f, 0f);

        // Reset text position
        textRectTransform.position.Set(0f, textRectTransform.position.y, textRectTransform.position.z);

        ScrollbarToBottom();

        m_LineCount = 0;
        float textTotalHeight = consoleText.GetComponent<RectTransform>().sizeDelta.y;
        float lineHeight = consoleText.preferredHeight;
        m_MaxLines = (int)(textTotalHeight / lineHeight);

        // Leave some space at the top 
        m_MaxLines--;

        consoleScrollView.SetActive(false);
    }

    public void Shutdown() {

    }

    public void OutputString(string message) {
        int numLines = message.Split('\n').Length;
        m_LineCount += numLines;

        // Truncate oldest lines if overflowing 
        if (m_LineCount > m_MaxLines) {
            consoleText.text = consoleText.text.Substring(GetLineIndex(m_LineCount - m_MaxLines) + 1);
            m_LineCount = m_MaxLines;
        }

        consoleText.text += message + "\n";

        ScrollbarToBottom();
    }

    void ScrollbarToBottom() {
        consoleScrollbar.value = 0f;
    }

    int GetLineIndex(int numLines) {
        string text = consoleText.text;
        int count = 0;
        for (int i = 0; i < text.Length; i++) {
            if (text[i] == '\n') {
                count++;
                if (count == numLines) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void Update() {
        if (Input.GetButtonDown("Console")) {
            consoleScrollView.SetActive(!consoleScrollView.activeSelf);
        }
    }
}
