﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidodyExtension {

    static Dictionary<int, Vector3> pausedState = new Dictionary<int, Vector3>();

    public static void Pause(this Rigidbody rigidbody) {
        int id = rigidbody.GetInstanceID();
        Vector3 velocity = rigidbody.velocity;
        pausedState[id] = velocity;
        rigidbody.Sleep();
    }

    public static void Unpause(this Rigidbody rigidbody) {
        rigidbody.velocity = pausedState[rigidbody.GetInstanceID()];
        rigidbody.WakeUp();
    }
}
