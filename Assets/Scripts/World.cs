﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.Networking.Transport;

class World {

    Dictionary<int, GameObject> m_Entities;
    bool m_ClientIsServer;

    int nextId = 0;

    public World(bool isClientSameAsServer = false) {
        // Only used for client world
        m_ClientIsServer = isClientSameAsServer;
    }

    public void Init() {
        m_Entities = new Dictionary<int, GameObject>();
    }

    public int SpawnEntity(GameObject prefab, Vector3 position, int netId = -1) {
        GameObject throwaway = null;
        return SpawnEntity(prefab, position, Quaternion.identity, ref throwaway, netId);
    }

    public int SpawnEntity(GameObject prefab, Vector3 position, ref GameObject spawnedEntity, int netId = -1) {
        return SpawnEntity(prefab, position, Quaternion.identity, ref spawnedEntity, netId);
    }

    public int SpawnEntity(GameObject prefab, Vector3 position, Quaternion rotation, ref GameObject spawnedEntity, int netId = -1) {
        if (!m_ClientIsServer) {
            if (!prefab.GetComponent<NetworkIdentifier>()) {
                GameDebug.LogError("Tried to spawn un-networked prefab: " + prefab);
                return -1;
            }

            spawnedEntity = UnityEngine.GameObject.Instantiate(prefab, position, Quaternion.identity);

            if (netId == -1) {
                netId = nextId;
                nextId++;
            }

            spawnedEntity.GetComponent<NetworkIdentifier>().networkId = netId;
            m_Entities[netId] = spawnedEntity;
            return netId;
        }
        return -1;
    }

    public GameObject GetEntity(int netId) {
        if (m_Entities.ContainsKey(netId)) {
            return m_Entities[netId];
        }
        return null;
    }

    public void GetSnapshot(ref DataStreamWriter streamWriter) {
        streamWriter.Write(m_Entities.Count);
        foreach (KeyValuePair<int, GameObject> entity in m_Entities) {
            streamWriter.Write(entity.Key);
            streamWriter.Write(entity.Value.transform.position.x);
            streamWriter.Write(entity.Value.transform.position.y);
            streamWriter.Write(entity.Value.transform.position.z);
            GameDebug.Log(string.Format("Sending new position: {0}, {1}, {2}", entity.Value.transform.position.x, entity.Value.transform.position.y, entity.Value.transform.position.z));
        }
    }

    public void ReadSnapshot(ref DataStreamReader streamReader, ref DataStreamReader.Context readerContext) {
        int size = streamReader.ReadInt(ref readerContext);
        for (int i = 0; i < size; i++) {
            int netId = streamReader.ReadInt(ref readerContext);
            float posX = streamReader.ReadFloat(ref readerContext);
            float posY = streamReader.ReadFloat(ref readerContext);
            float posZ = streamReader.ReadFloat(ref readerContext);

            GameDebug.Log(string.Format("Retrieved new position for {3}: {0}, {1}, {2}", posX, posY, posZ, netId));

            if (!m_Entities.ContainsKey(netId)) {
                SpawnEntity(DefaultPrefab(), Vector3.zero, netId);
            }

            Movable movable = m_Entities[netId].GetComponent<Movable>();
            if (!movable) {
                GameDebug.LogWarning("Received entity update for unmovable entity: " + netId);
                continue;
            }

            movable.SetPosition(posX, posY, posZ);
        }
    }

    private GameObject DefaultPrefab() {
        return Resources.Load<GameObject>("player");
    }
}
