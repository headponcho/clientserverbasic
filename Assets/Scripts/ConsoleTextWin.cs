﻿using System.IO;
using System.Runtime.InteropServices;

class ConsoleTextWin : IConsoleUI {

    [DllImport("Kernel32.dll")]
    private static extern bool AllocConsole();

    [DllImport("Kernel32.dll")]
    private static extern bool FreeConsole();

    [DllImport("Kernel32.dll")]
    private static extern bool SetConsoleTitle(string title);

    string m_ConsoleTitle;
    TextWriter m_PrevOutput;

    public ConsoleTextWin(string consoleTitle) {
        m_ConsoleTitle = consoleTitle;
    }

    public void Init() {
        AllocConsole();
        SetConsoleTitle(m_ConsoleTitle);

        System.Console.BackgroundColor = System.ConsoleColor.DarkBlue;

        System.Console.Clear();
        m_PrevOutput = System.Console.Out;
        System.Console.SetOut(new StreamWriter(System.Console.OpenStandardOutput()) { AutoFlush = true });
    }

    public void Shutdown() {
        OutputString("Console shutdown.");
        System.Console.SetOut(m_PrevOutput);
        FreeConsole();
    }

    public void OutputString(string message) {
        System.Console.WriteLine(message);
    }
}
