﻿using System.Net;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;

using NetworkConnection = Unity.Networking.Transport.NetworkConnection;
using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

class ServerGameLoop : Game.IGameLoop {

    UdpCNetworkDriver m_Socket;
    private List<NetworkConnection> m_Connections;
    private List<NetworkConnection> m_RemovedConnections;

    private Dictionary<int, Movable> m_ConToPlayer;

    World world;

    float m_ElapsedTime;
    bool m_IsListenServer;
    uint m_Tick;

    public bool Init(string[] args) {
        List<string> serverInitArguments = new List<string>(args);
        
        m_Socket = new UdpCNetworkDriver(new INetworkParameter[0]);
        int port = NetworkConfig.defaultServerPort;
        if (m_Socket.Bind(new IPEndPoint(IPAddress.Any, port)) != 0) {
            GameDebug.Log(string.Format("Failed to bind to port {0}.", port));
        } else {
            m_Socket.Listen();
            GameDebug.Log(string.Format("Listening on port {0}.", port));
        }

        m_Connections = new List<NetworkConnection>();
        m_RemovedConnections = new List<NetworkConnection>();

        m_ConToPlayer = new Dictionary<int, Movable>();

        m_IsListenServer = serverInitArguments.Contains("-listen");

        if (m_IsListenServer) {
            m_ElapsedTime = NetworkConfig.msPerTick;
        }

        m_Tick = 0;

        world = new World();
        world.Init();

        return true;
    }

    public void Shutdown() {
        m_Socket.Dispose();
        m_Connections.Clear();
    }

    public void Update(float deltaTime) {
        // Only manually manage tick rate if running listen server
        if (m_IsListenServer) {
            m_ElapsedTime += deltaTime;
            if (m_ElapsedTime < NetworkConfig.msPerTick) {
                return;
            }

            m_ElapsedTime -= NetworkConfig.msPerTick;
            if (m_ElapsedTime >= NetworkConfig.msPerTick) {
                m_ElapsedTime = 0f;
            }
        }

        m_Socket.ScheduleUpdate().Complete();

        AcceptConnections();

        ReadNetworkEvents();

        RemoveDisconnectedClients();

        UpdateClients();

        m_Tick++;
    }

    public void FixedUpdate() {

    }

    public void LateUpdate() {

    }

    void AcceptConnections() {
        while (true) {
            NetworkConnection con = m_Socket.Accept();
            if (!con.IsCreated) {
                break;
            }
            m_Connections.Add(con);
            GameDebug.Log(string.Format("Accepting connection: {0}.", con.InternalId));
            OnConnect(con);
        }
    }

    void ReadNetworkEvents() {
        foreach (NetworkConnection con in m_Connections) {
            DataStreamReader stream;
            NetworkEvent.Type type;
            while ((type = m_Socket.PopEventForConnection(con, out stream)) != NetworkEvent.Type.Empty) {
                if (type == NetworkEvent.Type.Data) {
                    OnData(con.InternalId, stream);
                } else if (type == NetworkEvent.Type.Disconnect) {
                    m_RemovedConnections.Add(con);
                    GameDebug.Log(string.Format("Client disconnected: {0}.", con.InternalId));
                }
            }
        }
    }

    void RemoveDisconnectedClients() {
        foreach (NetworkConnection con in m_RemovedConnections) {
            m_Connections.Remove(con);
        }
        m_RemovedConnections.Clear();
    }

    void UpdateClients() {
        foreach (NetworkConnection con in m_Connections) {
            // 8 = header + arraysize, 
            // 16 = per entity, 
            // 1 = # of entities
            DataStreamWriter worldSnapshotData = new DataStreamWriter(8 + (16 * 2), Allocator.Temp);
            // Header
            byte worldSnapshotPacketId = 1;
            worldSnapshotData.Write(worldSnapshotPacketId);
            // Payload
            world.GetSnapshot(ref worldSnapshotData);
            con.Send(m_Socket, worldSnapshotData);
            worldSnapshotData.Dispose();
        }
    }

    void OnConnect(NetworkConnection con) {
        GameObject newPlayerGO = null;
        int netId = world.SpawnEntity(Resources.Load<GameObject>("player"), new Vector3(0f, 3f, 0f), ref newPlayerGO);
        m_ConToPlayer.Add(con.InternalId, newPlayerGO.GetComponent<Movable>());

        SendConnectAcceptPacket(con, netId);
    }

    void OnData(int conId, DataStreamReader stream) {
        DataStreamReader.Context readerContext = default(DataStreamReader.Context);
        uint tick = stream.ReadUInt(ref readerContext);
        byte input = stream.ReadByte(ref readerContext);

        byte forwardMask = 1 << 0;
        byte backwardMask = 1 << 1;
        byte leftMask = 1 << 2;
        byte rightMask = 1 << 3;

        UserCommand playerInput = new UserCommand {
            forward = (input & forwardMask) == forwardMask,
            backward = (input & backwardMask) == backwardMask,
            left = (input & leftMask) == leftMask,
            right = (input & rightMask) == rightMask
        };

        m_ConToPlayer[conId].Move(playerInput);
    }

    void SendConnectAcceptPacket(NetworkConnection con, int netId) {
        DataStreamWriter clientIdData = new DataStreamWriter(12, Allocator.Temp);
        // Header
        byte clientIdPacketId = 0;
        clientIdData.Write(clientIdPacketId);
        clientIdData.Write(m_Tick);
        // Payload
        clientIdData.Write(netId);
        con.Send(m_Socket, clientIdData);
        clientIdData.Dispose();
    }
}
