﻿using System;
using System.Collections.Generic;

public interface IConsoleUI {
    void Init();
    void Shutdown();
    void OutputString(string message);
}

public class Console {
    static IConsoleUI s_ConsoleUI;

    public static void Init(IConsoleUI consoleUI) {
        s_ConsoleUI = consoleUI;
        s_ConsoleUI.Init();
        Write("Console ready.");
    }

    public static void Shutdown() {
        s_ConsoleUI.Shutdown();
    }

    public static void Write(string message) {
        s_ConsoleUI.OutputString(message);
    }
}

public class ConsoleNullUI : IConsoleUI {
    public void Init() {}

    public void OutputString(string message) {}

    public void Shutdown() {}
}
