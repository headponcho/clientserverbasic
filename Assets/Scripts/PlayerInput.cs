﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct UserCommand {
    public bool forward;
    public bool backward;
    public bool left;
    public bool right;
}

public class PlayerInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

    }

    public UserCommand Sample() {
        UserCommand cmd = new UserCommand();

        float horizontal = Input.GetAxis("Horizontal");
        if (!(horizontal < float.Epsilon && horizontal > -float.Epsilon)) {
            if (horizontal < 0f) {
                cmd.left = true;
            } else {
                cmd.right = true;
            }
        }

        float vertical = Input.GetAxis("Vertical");
        if (!(vertical < float.Epsilon && vertical > -float.Epsilon)) {
            if (vertical < 0f) {
                cmd.backward = true;
            } else {
                cmd.forward = true;
            }
        }

        return cmd;
    }
}
