﻿
public static class NetworkConfig {

    public const int defaultServerPort = 9000;
    public const int tickRate = 50;
    public static float msPerTick = 1f / tickRate;
}