﻿using System;
using System.Net;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Networking.Transport;
using UnityEngine;

using NetworkConnection = Unity.Networking.Transport.NetworkConnection;
using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

class ClientGameLoop : Game.IGameLoop {

    UdpCNetworkDriver m_Socket;
    NetworkConnection m_ClientToServerConnection;

    World world;

    float m_ElapsedTime;
    uint m_Tick;
    bool m_IsConnected;
    bool m_IsListenServer;

    PlayerInput m_PlayerInput;

    public bool Init(string[] args) {
        List<string> clientInitArguments = new List<string>(args);

        m_Socket = new UdpCNetworkDriver(new INetworkParameter[0]);
        IPAddress address = IPAddress.Loopback;
        if (clientInitArguments.Contains("-ip")) {
            string ip = clientInitArguments[clientInitArguments.IndexOf("-ip") + 1];
            address = IPAddress.Parse(ip);
        }
        int port = NetworkConfig.defaultServerPort;
        m_ClientToServerConnection = m_Socket.Connect(new IPEndPoint(address, port));

        if (!m_ClientToServerConnection.IsCreated) {
            m_ClientToServerConnection = default(NetworkConnection);
            GameDebug.Log(string.Format("Failed to connect to {0}:{1}.", address, port));
            return false;
        } else {
            GameDebug.Log(string.Format("Connected to {0}:{1}.", address, port));
        }

        m_ElapsedTime = NetworkConfig.msPerTick;
        m_IsConnected = false;

        m_IsListenServer = clientInitArguments.Contains("-listen");

        world = new World(m_IsListenServer);
        world.Init();

        return true;
    }

    public void Shutdown() {
        m_ClientToServerConnection.Disconnect(m_Socket);
        m_ClientToServerConnection = default(NetworkConnection);
        m_Socket.Dispose();
    }

    public void Update(float deltaTime) {
        m_ElapsedTime += deltaTime;
        if (m_ElapsedTime < NetworkConfig.msPerTick) {
            return;
        }

        m_ElapsedTime -= NetworkConfig.msPerTick;
        if (m_ElapsedTime >= NetworkConfig.msPerTick) {
            m_ElapsedTime = 0f;
        }

        m_Socket.ScheduleUpdate().Complete();

        ReadNetworkEvents();

        SendClientUpdate();

        m_Tick++;
    }

    public void FixedUpdate() {

    }

    public void LateUpdate() {

    }

    void ReadNetworkEvents() {
        DataStreamReader stream;
        NetworkEvent.Type type;
        while ((type = m_ClientToServerConnection.PopEvent(m_Socket, out stream)) != NetworkEvent.Type.Empty) {
            if (type == NetworkEvent.Type.Connect) {
                GameDebug.Log("Client received CONNECT event.");
            } else if (type == NetworkEvent.Type.Disconnect) {
                GameDebug.Log("Client received DISCONNECT event.");
                m_IsConnected = false;
            } else if (type == NetworkEvent.Type.Data) {
                OnData(stream);
            }
        }
    }

    void OnData(DataStreamReader stream) {
        CheckIfFirstPacket(stream);

        if (m_IsConnected && !m_IsListenServer) {
            DataStreamReader.Context readerContext = default(DataStreamReader.Context);
            byte packetId = stream.ReadByte(ref readerContext);

            // 1 = world snapshot
            if (packetId == 1) {
                world.ReadSnapshot(ref stream, ref readerContext);
            }
        }
    }

    void CheckIfFirstPacket(DataStreamReader stream) {
        if (!m_IsConnected) {
            DataStreamReader.Context readerContext = default(DataStreamReader.Context);
            byte packetId = stream.ReadByte(ref readerContext);
            // 0 = Connection accepted
            if (packetId == 0) {
                m_Tick = stream.ReadUInt(ref readerContext);

                int netId = stream.ReadInt(ref readerContext);

                GameObject player = null;
                world.SpawnEntity(Resources.Load<GameObject>("player"), new Vector3(0f, 3f, 0f), ref player, netId);
                if (!player && m_IsListenServer) {
                    // TODO: Allow access to server World to get new player object instead of using this loop
                    foreach (NetworkIdentifier networkedObject in UnityEngine.GameObject.FindObjectsOfType<NetworkIdentifier>()) {
                        if (networkedObject.networkId == netId) {
                            player = networkedObject.gameObject;
                            break;
                        }
                    }
                }

                m_PlayerInput = player.AddComponent<PlayerInput>();

                m_IsConnected = true;

                GameDebug.Log(string.Format("Client assigned ID: {0}. Starting ticking at: {1}.", netId, m_Tick));
            }
        }
    }

    void SendClientUpdate() {
        if (m_IsConnected) {
            DataStreamWriter updateData = new DataStreamWriter(8, Allocator.Temp);
            // Header
            updateData.Write(m_Tick);

            // Input
            UserCommand cmd = m_PlayerInput.Sample();
            byte input = 0;
            if (cmd.forward) {
                input |= 1 << 0;
            }
            if (cmd.backward) {
                input |= 1 << 1;
            }
            if (cmd.left) {
                input |= 1 << 2;
            }
            if (cmd.right) {
                input |= 1 << 3;
            }
            updateData.Write(input);

            m_ClientToServerConnection.Send(m_Socket, updateData);
            updateData.Dispose();
        }
    }
}
