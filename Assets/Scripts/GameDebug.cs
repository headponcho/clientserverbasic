﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameDebug {

    static bool m_ForwardToEditor;

    public static void Init() {
        m_ForwardToEditor = Application.isEditor;
        GameDebug.Log("GameDebug initialized.");
    }

    public static void Shutdown() {}

    public static void Log(string message) {
        if (m_ForwardToEditor) {
            Debug.Log(message);
        } else {
            Console.Write(Time.frameCount + ": " + message);
        }
    }

    public static void LogError(string message) {
        if (m_ForwardToEditor) {
            Debug.LogError(message);
        } else {
            Console.Write(Time.frameCount + ": [ERR] " + message);
        }
    }

    public static void LogWarning(string message) {
        if (m_ForwardToEditor) {
            Debug.LogWarning(message);
        } else {
            Console.Write(Time.frameCount + ": [WARN] " + message);
        }
    }
}
